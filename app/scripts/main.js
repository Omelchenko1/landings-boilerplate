$(function () {
  $('.up-btn').handleUpbtn({
    'text': 'К началу страницы'
  });


  !(function () {
    $('.tabs').tabs();
  })();


  /* language block, accessibility handling closure */

  !(function () {
    $('.lang').focus(function () {
      $(this).addClass('focus');
    }).on('mouseout', function () {
      $(this).removeClass('focus');
    });

    $('.lang__item').focus(function () {
      $('.lang').addClass('focus');
    }).blur(function () {
      $('.lang').removeClass('focus');
    });
  })();


  /* modal window initialisation closure */

  !(function initModal() {
    var modaalOptions = {
      background_scroll: true,
      overlay_opacity: 0.6,
    },
    modaalOptionsRu = {
      accessible_title: "Диалоговое окно",
      close_text: "Закрыть",
      close_aria_label: "Закрыть (нажмите Esc для закрытия)"
    },
    modaalOptionsUa = {
      accessible_title: "Діалогове вікно",
      close_text: "Закрити",
      close_aria_label: "Закрити (натисніть Esc для закриття)"
    };

    if( $('html').attr('lang') === 'uk' ) {
      $.extend(modaalOptions, modaalOptionsUa);
    } else {
      $.extend(modaalOptions, modaalOptionsRu);
    }

    $('.modal').modaal(modaalOptions);
  })();


  /* accordion initialisation closure */

  !(function initAccordion() {
    var myAccordion = Fraccordion({
      selector: '.accordion',
      headerSelector: '.accordion__head',
      headerIdPrefix: 'accordion-header',
      panelSelector: '.accordion__body',
      panelIdPrefix: 'accordion-panel',
      firstPanelsOpenByDefault: false,
      multiselectable: false,
      readyClass: 'accordion--is-ready'
    });

    myAccordion.init();
  })();
});
